package constants;

import api.utils.UtilsMethod;

import static constants.Constants.Servers.*;

public class Constants {

    public static class RunVariable {
        public static String server = JSON_PLACEHOLDER_URL;
        public static String path = "";
    }

    //domain name
    public static class Servers {
        public static String SWAPI_URL = "https://swapi.dev/";
        public static String GOOGLE_PLACES_SERVER = "https://maps.googleapis.com/";
        public static String JSON_PLACEHOLDER_URL="https://jsonplaceholder.typicode.com/";
        public static String REQUESTBIN_URL = "https://eoeuujm6u6vfbbp.m.pipedream.net";
    }

    //path
    public static class Path {
        public static String SWAPI_PATH = "api/";
        public static String GOOGLE_PLACES_PATH = "maps/api/place/";
    }

    //endpoint
    public static class Endpoint {
        //swapi
        public static String SWAPI_GET_PEOPLE = "people/";
        //google places
        public static String GOOGLE_PLACES_ENDPOINT_SEARCH = "findplacefromtext/json";
        //json placeholder
        public static String JSONPLACEHOLDER_GET = "/comments";
        public static String JSONPLACEHOLDER_PUT = "posts/1";
        public static String JSONPLACEHOLDER_DELETE = "posts/1";
        public static String JSONPLACEHOLDER_POST = "posts";
    }

    public static final String API_TOKEN = UtilsMethod.getValue("TOKEN");
}
